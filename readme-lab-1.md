# Team and project
## Team code/name

>Група №16

## Team members list 
1. > Вронський В.В. ІС-92 
2. > Машков А.В. ІА-91 

## Unity version
> Unity (2020.3.21f1)

</br>

# Lab
## About team task management (screenshot/description)
>Trello
>![alt text](screenshots/trello.png "Our Board")

## Chosen games analysis
## Игра: Hearts of Iron IV
# Геймплей :
Стратегия реального времени, игра за государство, управление через события и управление непосредственно войсками, перемещая их по клеткам(провинциям).
# Сюжет:
Вторая мировая с возможностью менять историю
# Графика 
псевдо 2д карта, эффект 3д создаётся наложенной картой нормалей, а также расставленные на ней 3д модели